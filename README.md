# README #

C++ implementation of the Approximate String Online Matching (ASOM) algorithm. Given a stream **t**, a pattern **p** to be searched and a tolerance value *k*, the output is a set of non-overlapping subsequences **s** of **t**, so that the Levenshtein distance *d*(**s**,**p**) <= *k*. 
The algorithm is also available in a [MATLAB implementation](https://bitbucket.org/slackericida/asom-matlab/overview).

### How do I get set up? ###

* The program uses the SPARE library, which can be downloaded [here](http://sourceforge.net/p/libspare/home/Spare/) and must be installed before running the program.
* The program reads the text file "test_file", for retrieving the sequence **t** and the pattern **p** to be searched. Modify this file in order to change the parameters. The first line contains the sequence **t**, while the second line the pattern **p**.

### Execution ###
Run the function "main.cpp", which calls the function 
```
#!c++
wr2.Process(t,p,k);
```
where k is the maximum tolerance admitted in the Levenshtein dissimilarity. The output returned is the set of the occurrences found, defined as a list of pairs which contain the initial position in **t** and the length of the occurrence.

### Related literature ###

A pre-print of the article relative to the algorithm can be found at the [following link](https://google.com). Please, refer to it for a detailed description and explanation of the procedure.

### Contact Information ###

* Filippo Maria Bianchi
* E-mail: filippombianchi@gmail.com