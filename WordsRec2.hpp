/*
 * WordsRec2.hpp
 *      Author: Filippo Maria Bianchi
 *      Email: filippombianchi@google.com
 */

#ifndef WORDSREC2_HPP_
#define WORDSREC2_HPP_

#include <spare/SpareTypes.hpp>
using namespace spare;
using namespace std;

template<class Dissimilarity, class DataType>
class WordsRec2{

public:

    typedef pair<IntegerType,IntegerType> occurrencyType;

    vector<pair<IntegerType,IntegerType> >& getOccurrencies(){
        return Occurrencies;
    }

    template<typename sequenceContainer>
    void Process(const sequenceContainer& seq, const sequenceContainer& word, IntegerType tol);

    WordsRec2(){
        empty = 1;
    }

private:

    //vector containing the occurrences returned
    vector<occurrencyType> Occurrencies;

    bool empty;

};

template<class Dissimilarity, class DataType>
template<typename sequenceContainer>
void WordsRec2<Dissimilarity, DataType>::Process(const sequenceContainer& seq, const sequenceContainer& word, IntegerType tol){

    Occurrencies.clear();
    Dissimilarity mDiss;

    IntegerType Lword = word.size();
    IntegerType Lmin = Lword-tol;
    IntegerType Lmax = Lword+tol;

    //integrity check
    if(tol >= Lword){
        cout << "Tolerance too high" << endl;
        return;
    }

    IntegerType Lseq = seq.size();

    //integrity check
    if(Lseq <= Lword){
        cout << "Sequence too short" << endl;
        return;
    }

    vector<vector<occurrencyType> > Hits;
    Hits.resize(tol+1);
    IntegerType pos=0;

    typedef typename sequenceContainer::const_iterator sequenceContainerIT;
    sequenceContainerIT wordITstart = word.begin(), wordITend = word.end(),
                        matchITstart=seq.begin(), matchITend;


    //browse the whole sequence looking for occurrences and stop as soon as the minimum length mask would end beyond the sequence
    while(pos <= IntegerType(Lseq-Lmin)){

        //starting from the current position, try every mask
        for(IntegerType n = Lmin;n <= Lmax; n++){

            //check if the mask ends after the end of the sequence
            if(pos+n <= IntegerType(Lseq) ){
                matchITend = matchITstart;
                advance(matchITend,n);

                //evaluate the dissimilarity of the subsequence from the prototype
                IntegerType diss = mDiss.Diss(make_pair(matchITstart, matchITend), make_pair(wordITstart, wordITend));

                //if the dissimilarity d of the subsequence from the prototype is < tol, save the subsequence in the container d
                if(diss <= tol)
                    Hits[diss].push_back(make_pair(pos,n));

            }
        }

        /*
        * as the current position pos advance, check if is it possible to return any of the subsequences found. A subsequence can be
        * returned if it started on a position sufficient far from pos and it has not been removed from Hits.
        */
        vector<vector<occurrencyType> >::iterator HitsIT1 = Hits.begin(), HitsIT2;
        IntegerType d=0;
        while(HitsIT1 != Hits.end()){

            //check if the container relative to the distance 'd' contains at least one subsequence
            if((*HitsIT1).size() > 0){

                //select the first element (the next ones started in a later position)
                occurrencyType subseq_d = (*(*HitsIT1).begin());

                //check if subseq_d can be returned
                if(subseq_d.first == pos - (Lword*d + d*(d-1)/2 + Lword)){

                    Occurrencies.push_back(subseq_d);
                    (*HitsIT1).erase((*HitsIT1).begin());

                    //remove all the subsequences with higher distance overlapping with subseq_d
                    HitsIT2 = HitsIT1;
                    while(HitsIT2 != Hits.end()){
                        vector<occurrencyType>::iterator vIT = (*HitsIT2).begin();
                        while(vIT != (*HitsIT2).end()){
                            occurrencyType subseq_i = *vIT;

                            //check if is it possible to remove the subsequence subseq_i (if it is overlapping with subseq_d)
                            if(!( (subseq_i.first+subseq_i.second <= subseq_d.first) || (subseq_d.first+subseq_d.second) <= subseq_i.first))
                                vIT = (*HitsIT2).erase(vIT);
                            else
                                vIT++;
                        }

                        HitsIT2++;
                    }
                }
            }

            HitsIT1++;
            d++;
        }

        pos++;
        matchITstart++;
    }

    if(empty){
        //return what is left in Hits
        vector<vector<occurrencyType > >::iterator HitsIT1 = Hits.begin(), HitsIT2;
        while(HitsIT1 != Hits.end()){
            vector<occurrencyType >::iterator vIT = (*HitsIT1).begin();
            while(vIT != (*HitsIT1).end()){

                //return the subsequence subseq_1 with minor distance from the pattern, among the ones left in Hits
                occurrencyType subseq_1 = *vIT++;
                Occurrencies.push_back(subseq_1);

                //remove all the subsequences in Hits, with a distance greater than subseq_1 and overlapping with it
                vector<occurrencyType >::iterator vIT2 = vIT;
                HitsIT2 = HitsIT1;
                while(HitsIT2 != Hits.end()){
                    while(vIT2 != (*HitsIT2).end()){
                        occurrencyType subseq_2 = *vIT2;
                        if(!( (subseq_1.first+subseq_1.second <= subseq_2.first) || (subseq_2.first+subseq_2.second) <= subseq_1.first)){
                            (*HitsIT2).erase(vIT2);
                        }
                        else
                           vIT2++;
                    }
                    HitsIT2++;
                    vIT2 = (*HitsIT2).begin();
                }
            }
            HitsIT1++;
        }
    }

}



#endif /* WORDSREC2_HPP_ */
