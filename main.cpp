/*
 * main.cpp
 *      Author: Filippo Maria Bianchi
 *      Email: filippombianchi@google.com
 */

#include <spare/Dissimilarity/Levenshtein.hpp>
#include "WordsRec.hpp"
#include "WordsRec2.hpp"
#include <fstream>
#include <sstream>
#include <string>
using namespace std;
using namespace spare;

int main(){

    string s1,s2;
    ifstream infile;
    infile.open("test_file", ios::in);

    getline(infile, s1); //sequence
    getline(infile, s2); //pattern

    WordsRec2<Levenshtein, string> wr2;
    wr2.Process(s1,s2,2); //find the occurrences of s1 in s2, admitting an edit distance <= 2

    typedef WordsRec2<Levenshtein, string>::occurrencyType occurrency2;
    vector<occurrency2> occurrencies2 = wr2.getOccurrencies();

    cout<<occurrencies2.size()<<" words found:"<<endl;
    vector<occurrency2>::iterator occIT2 = occurrencies2.begin();
    while(occIT2 != occurrencies2.end()){
        occurrency2 occ = *occIT2++;
        std::string str2 = s1.substr (occ.first,occ.second);
        cout << str2 <<" -- pos: "<<occ.first<<endl;
    }

    return EXIT_SUCCESS;
}


